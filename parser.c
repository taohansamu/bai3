/* 
 * @copyright (c) 2008, Hedspi, Hanoi University of Technology
 * @author Huu-Duc Nguyen
 * @version 1.0
 */

#include <stdlib.h>

#include "reader.h"
#include "scanner.h"
#include "parser.h"
#include "error.h"

Token *currentToken;
Token *lookAhead;

void scan(void) {
  Token* tmp = currentToken;
  currentToken = lookAhead;
  lookAhead = getValidToken();
  free(tmp);
}

void eat(TokenType tokenType) {
  if (lookAhead->tokenType == tokenType) {
    printToken(lookAhead);
    scan();
  } else missingToken(tokenType, lookAhead->lineNo, lookAhead->colNo);
}

void compileProgram(void) {
  assert("Parsing a Program ....");
  eat(KW_PROGRAM);
  eat(TK_IDENT);
  eat(SB_SEMICOLON);
  compileBlock();
  eat(SB_PERIOD);
  assert("Program parsed!");
}

void compileBlock(void) {
  assert("Parsing a Block ....");
  if (lookAhead->tokenType == KW_CONST) {
    eat(KW_CONST);
    compileConstDecl();
    compileConstDecls();
    compileBlock2();
  } 
  else compileBlock2();
  assert("Block parsed!");
}

void compileBlock2(void) {
  if (lookAhead->tokenType == KW_TYPE) {
    eat(KW_TYPE);
    compileTypeDecl();
    compileTypeDecls();
    compileBlock3();
  } 
  else compileBlock3();
}

void compileBlock3(void) {
  if (lookAhead->tokenType == KW_VAR) {
    eat(KW_VAR);
    compileVarDecl();
    compileVarDecls();
    compileBlock4();
  } 
  else compileBlock4();
}

void compileBlock4(void) {
  compileSubDecls();
  compileBlock5();
}

void compileBlock5(void) {
  eat(KW_BEGIN);
  compileStatements();
  eat(KW_END);
}

void compileConstDecls(void) {
  // TODO
  if(lookAhead->tokenType==TK_IDENT)
  {
    compileConstDecl();
    compileConstDecls();
  }
}

void compileConstDecl(void) {
  // TODO
  //12) ConstDecl ::= Ident SB_EQ Constant SB_SEMICOLON
  eat(TK_IDENT);
  eat(SB_EQ);
  compileConstant();
  eat(SB_SEMICOLON);
}

void compileTypeDecls(void) {
  // TODO
  /*
10) ConstDecls::= ConstDecl ConstDecls
11) ConstDecls::= 
  */
  if(lookAhead->tokenType==TK_IDENT)
  {
    compileTypeDecl();  
    compileTypeDecls();
  } 
}

void compileTypeDecl(void) {
  // TODO TypeDecl ::= Ident SB_EQ Type SB_SEMICOLON
  eat(TK_IDENT);
  eat(SB_EQ);
  compileType();
  eat(SB_SEMICOLON);
}

void compileVarDecls(void) {
  // TODO16) VarDecls
//VarDecls ::= VarDecl VarDecls
 if(lookAhead->tokenType==TK_IDENT)
 {
   compileVarDecl();
   compileVarDecls();
 }
}

void compileVarDecl(void) {
  // TODO
  //VarDecl ::= Ident SB_COLON Type SB_SEMICOLON
  eat(TK_IDENT);
  eat(SB_COLON);
  compileType();
  eat(SB_SEMICOLON);

}

void compileSubDecls(void) {
  assert("Parsing subtoutines ....");
  // TODO

/*
19) SubDecls::= FunDecl SubDecls
20) SubDecls::= ProcDecl SubDecls
21) SubDecls::=  
*/
  if(lookAhead->tokenType==KW_FUNCTION)
  {
  compileFuncDecl();
  compileSubDecls(); 
  }
  if(lookAhead->tokenType==KW_PROCEDURE)
  {
    compileProcDecl();
    compileSubDecls();
  }
  assert("Subtoutines parsed ....");
}

void compileFuncDecl(void) {
  assert("Parsing a function ....");
  // TODO
/*  FunDecl ::= KW_FUNCTION Ident Params SB_COLON BasicType SB_SEMICOLON
Block SB_SEMICOLON*/
  eat(KW_FUNCTION);
  eat(TK_IDENT);
  compileParams();
  eat(SB_COLON);
  compileBasicType();
  eat(SB_SEMICOLON);
  compileBlock();
  eat(SB_SEMICOLON);
  assert("Function parsed ....");
}

void compileProcDecl(void) {
  assert("Parsing a procedure ....");
  // TODO
  //ProcDecl ::= KW_PROCEDURE Ident Params SB_SEMICOLON Block SB_SEMICOLON
  eat(KW_PROCEDURE);
  eat(TK_IDENT);
  compileParams();
  eat(SB_SEMICOLON);
  compileBlock();
  eat(SB_SEMICOLON);
  assert("Procedure parsed ....");
}

void compileUnsignedConstant(void) {
  // TODO
/*  36) UnsignedConstant ::= Number
37) UnsignedConstant ::= ConstIdent
38) UnsignedConstant ::= ConstChar*/
switch(lookAhead->tokenType)
{
  case TK_NUMBER:eat(TK_NUMBER);break;
  case TK_IDENT:eat(TK_IDENT);break;
  case TK_CHAR:eat(TK_CHAR);break;
  default:
    error(ERR_INVALIDCONSTANT, lookAhead->lineNo, lookAhead->colNo);
    break;
}
}

void compileConstant(void) {
  // TODO
  /*40)
41)
42)
43)
Constant::=SB_PLUS Constant2
Constant::=SB_MINUS Constant2
Constant::=Constant2
Constant::=ConstChar
*/
switch(lookAhead->tokenType)
  {
    case SB_PLUS:
      eat(SB_PLUS);
      compileConstant2();
      break;
    case SB_MINUS:
      eat(SB_MINUS);
      compileConstant2();
      break;
    case TK_CHAR:
      eat(TK_CHAR);
      break;
    default:
      compileConstant2();
      break;
  }
}

void compileConstant2(void) {
  // TODO
  /*\
  44) Constant2::= ConstIdent
45) Constant2::= Number
  */
  switch(lookAhead->tokenType){
    case TK_NUMBER:
      eat(TK_NUMBER);
      break;
    case TK_IDENT:
      eat(TK_IDENT);
      break;
    default:
      error(ERR_INVALIDCONSTANT,lookAhead->lineNo,lookAhead->colNo);
      break;
  }
}

void compileType(void) {
  // TODO
  /*
  30)Type::=KW_INTEGER
31)Type::=KW_CHAR
32)Type::=TypeIdent
33)Type::=KW_ARRAY SB_LSEL Number SB_RSEL KW_OF Type*/
switch(lookAhead->tokenType){
  case KW_INTEGER:
    eat(KW_INTEGER);
    break;
  case KW_CHAR:
    eat(KW_CHAR);
    break;
  case KW_ARRAY:
    eat(KW_ARRAY);
    eat(SB_LSEL);
    eat(TK_NUMBER);
    eat(SB_RSEL);
    eat(KW_OF);
    eat(TK_IDENT);
    break;
  case TK_IDENT:
    eat(TK_IDENT);
    break;
  default:
    error(ERR_INVALIDTYPE,lookAhead->lineNo,lookAhead->colNo);
    break;
}
}

void compileBasicType(void) {
  // TODO
  /*
  34) BasicType ::= KW_INTEGER
35) BasicType ::= KW_CHAR
*/
switch(lookAhead->tokenType){
  case KW_INTEGER:
    eat(KW_INTEGER);
    break;
  case KW_CHAR:
    eat(KW_CHAR);
    break;
  default:
    error(ERR_INVALIDTYPE,lookAhead->lineNo,lookAhead->colNo);
    break;
  }
}

void compileParams(void) {
  // TODO
  /*
  24) Params::= SB_LPAR Param Params2 SB_RPAR
  25) Params  ::= 
  */
  if(lookAhead->tokenType==SB_LPAR)
  {
  eat(SB_LPAR);
  compileParam();
  compileParams2();
  eat(SB_RPAR);
  }
}

void compileParams2(void) {
  // TODO
  /*
  26) Params2 ::= SB_SEMICOLON Param Params2
27) Params2 ::= 
  */
if(lookAhead->tokenType==SB_SEMICOLON)
{
  eat(SB_SEMICOLON);
  compileParam();
  compileParams2();
}

}

void compileParam(void) {
  // TODO
  /*
  28) Param::= Ident SB_COLON BasicType
  29) Param::= KW_VAR Ident SB_COLON BasicType
  */
switch(lookAhead->tokenType)
{
  case TK_IDENT:
    eat(TK_IDENT);
    eat(SB_COLON);
    compileBasicType();
    break;
  case KW_VAR:
    eat(KW_VAR);
    eat(TK_IDENT);
    eat(SB_COLON);
    compileBasicType();
    break;
  default:
   error(ERR_INVALIDPARAM,lookAhead->lineNo,lookAhead->colNo);
   break;
}
}

void compileStatements(void) {
  // TODO
  /*
  46) Statements ::= Statement Statements2
  */
  compileStatement();
  compileStatements2();
}

// void compileStatements2(void) {
//   // TODO
//   /*
//   47) Statements2 ::= SB_SEMICOLON Statement Statement2
//   48) Statements2 ::= 
//   */
//   //example6.kpl bi loi o day
//   if(lookAhead->tokenType==SB_SEMICOLON || (lookAhead->tokenType!=SB_SEMICOLON && lookAhead->tokenType !=KW_END))
//   {
//     eat(SB_SEMICOLON);
//     compileStatement();
//     compileStatements2();
//   }
// }
void compileStatements2(void) {
  // TODO
  switch (lookAhead->tokenType) {
    case SB_SEMICOLON:
      eat(SB_SEMICOLON);
      compileStatement();
      compileStatements2();
      break;
    // Follow
    case KW_END:
      break;
    // Error
    default:
      eat(SB_SEMICOLON);
      break;
  }
}
void compileStatement(void) {
  /*
  49)Statement::=AssignSt
  50)Statement::CallSt
  51)Statement::GroupSt
  52)Statement::IfSt
  53)Statement::WhileSt
  54)Statement::ForSt
  55)Statement::
  */
  switch (lookAhead->tokenType) {
  case TK_IDENT:
    compileAssignSt();
    break;
  case KW_CALL:
    compileCallSt();
    break;
  case KW_BEGIN:
    compileGroupSt();
    break;
  case KW_IF:
    compileIfSt();
    break;
  case KW_WHILE:
    compileWhileSt();
    break;
  case KW_FOR:
    compileForSt();
    break;
    // EmptySt needs to check FOLLOW tokens
  case SB_SEMICOLON:
  case KW_END:
  case KW_ELSE:
    break;
    // Error occurs
  default:
    error(ERR_INVALIDSTATEMENT, lookAhead->lineNo, lookAhead->colNo);
    break;
  }
}

void compileAssignSt(void) {
  assert("Parsing an assign statement ....");
  // TODO
  /*

56) AssignSt ::= Variable SB_ASSIGN Expession
57) AssignSt ::= FunctionIdent SB_ASSIGN Expression
  */
  eat(TK_IDENT);
  if(lookAhead->tokenType==SB_LSEL)
  {
    compileIndexes();
  }
  eat(SB_ASSIGN);
  compileExpression();
  assert("Assign statement parsed ....");
}

void compileCallSt(void) {
  assert("Parsing a call statement ....");
  // TODO
  //58) CallSt ::= KW_CALL ProcedureIdent Arguments 
  eat(KW_CALL);
  eat(TK_IDENT);
  compileArguments();
  assert("Call statement parsed ....");
}

void compileGroupSt(void) {
  assert("Parsing a group statement ....");
  // TODO
  //59) GroupSt ::= KW_BEGIN Statements KW_END
  eat(KW_BEGIN);
  compileStatements();
  eat(KW_END);
  assert("Group statement parsed ....");
}

void compileIfSt(void) {
  assert("Parsing an if statement ....");
  eat(KW_IF);
  compileCondition();
  eat(KW_THEN);
  compileStatement();
  if (lookAhead->tokenType == KW_ELSE) 
    compileElseSt();
  assert("If statement parsed ....");
}

void compileElseSt(void) {
  eat(KW_ELSE);
  compileStatement();
}

void compileWhileSt(void) {
  assert("Parsing a while statement ....");
  // TODO
  /*
63) WhileSt::= KW_WHILE Condition KW_DO Statement
  */
  eat(KW_WHILE);
  compileCondition();
  eat(KW_DO);
  compileStatement();
  assert("While statement parsed ....");
}

void compileForSt(void) {
  assert("Parsing a for statement ....");
  // TODO
  /*
  64) ForSt::= KW_FOR VariableIdent SB_ASSIGN Expression KW_TO
              Expression KW_DO Statement
  */
  eat(KW_FOR);
  eat(TK_IDENT);
  eat(SB_ASSIGN);
  compileExpression();
  eat(KW_TO);
  compileExpression();
  eat(KW_DO);
  compileStatement();
  assert("For statement parsed ....");
}

void compileArguments(void) {
  // TODO
  /*
65) Arguments ::= SB_LPAR Expression Arguments2 SB_RPAR
66) Arguments ::= 
  */
  if(lookAhead->tokenType==SB_LPAR)
  {
    eat(SB_LPAR);
    compileExpression();
    compileArguments2();
    eat(SB_RPAR);
  }
}

void compileArguments2(void) {
  // TODO
  /*
  67) Arguments2::= SB_COMMA Expression Arguments2
68) Arguments2::= 
  */
  if(lookAhead->tokenType==SB_COMMA)
  {
    eat(SB_COMMA);
    compileExpression();
    compileArguments2();
  }
}

void compileCondition(void) {
  // TODO
  // 68) Condition ::= Expression Condition2
  compileExpression();
  compileCondition2();
}

void compileCondition2(void) {
  // TODO
  /*
69.Condition2::=SB_EQ Expression
70.Condition2::=SB_NEQ Expression
71.Condition2::=SB_LE Expression
72.Condition2::=SB_LT Expression
73.Condition2::=SB_GE Expression
74.Condition2::=SB_GT Expression
  */
switch(lookAhead->tokenType)
{
  case SB_EQ:
    eat(SB_EQ);
    compileExpression();
    break;
  case SB_NEQ:
    eat(SB_NEQ);
    compileExpression();
    break;
  case SB_LE:
    eat(SB_LE);
    compileExpression();
    break;
  case SB_LT:
    eat(SB_LT);
    compileExpression();
    break;
  case SB_GE:
    eat(SB_GE);
    compileExpression();
    break;
  case SB_GT:
    eat(SB_GT);
    compileExpression();
    break;
  default:
    error(ERR_INVALIDEXPRESSION,lookAhead->lineNo,lookAhead->colNo);
    break;
}
}

void compileExpression(void) {
  assert("Parsing an expression");
  // TODO
  /*
  75) Expression ::= SB_PLUS Expression2
  76) Expression ::= SB_MINUS Expression2
  77) Expression ::= Expression2
  */
switch(lookAhead->tokenType){
  case SB_PLUS:
    eat(SB_PLUS);
    compileExpression2();
    break;
  case SB_MINUS:
    eat(SB_MINUS);
    compileExpression2();
    break;
  default:
    compileExpression2();
    break;
}
  assert("Expression parsed");
}

void compileExpression2(void) {
  // TODO
  //78) Expression2 ::= Term Expression3
  compileTerm();
  compileExpression3();
}


void compileExpression3(void) {
  // TODO
  /*
79) Expression3 ::= SB_PLUS Term Expression3
80) Expression3 ::= SB_MINUS Term Expression3
81) Expression3 ::= 
  */
switch(lookAhead->tokenType)
{
  case SB_PLUS:
    eat(SB_PLUS);
    compileTerm();
    compileExpression3();
    break;
  case SB_MINUS:
    eat(SB_MINUS);
    compileTerm();
    compileExpression3();
    break;
  default:break;
}
}

void compileTerm(void) {
  // TODO
  /*
82) Term ::= Factor Term2
  */
  compileFactor();
  compileTerm2();
}

void compileTerm2(void) {
  // TODO
  /*
83) Term2 ::= SB_TIMES Factor Term2
84) Term2 ::= SB_SLASH Factor Term2
85) Term2 ::= 
  */
  switch(lookAhead->tokenType)
  {
    case SB_TIMES:
      eat(SB_TIMES);
      compileFactor();
      compileTerm2();
      break;
    case SB_SLASH:
      eat(SB_SLASH);
      compileFactor();
      compileTerm2();
      break;
    default:break;
  }
}

void compileFactor(void) {
  // TODO
  /*
86)Factor::=UnsignedConstant
87)Factor::=Variable
88)Factor::=FunctionApptication
89)Factor::=SB_LPAR Expression SB_RPAR
  */
 switch (lookAhead->tokenType) {
    case TK_NUMBER:
    case TK_CHAR:
      compileUnsignedConstant();
      break;
    case SB_LPAR:
      eat(SB_LPAR);
      compileExpression();
      eat(SB_RPAR);
      break;
    case TK_IDENT:
      eat(TK_IDENT);
      switch(lookAhead->tokenType) {
        case SB_LSEL:
          compileIndexes();
          break;
        case SB_LPAR:
          compileArguments();
          break;
        default:
          break;
      }
      break;
    default:
      error(ERR_INVALIDFACTOR, lookAhead->lineNo, lookAhead->colNo);
      break;
  }
}

void compileIndexes(void) {
  // TODO
//   92) Indexes ::= SB_LSEL Expression SB_RSEL Indexes
// 93) Indexes ::= 
  if(lookAhead->tokenType==SB_LSEL)
  {
    eat(SB_LSEL);
    compileExpression();
    eat(SB_RSEL);
    compileIndexes();
  }
}

int compile(char *fileName) {
  if (openInputStream(fileName) == IO_ERROR)
    return IO_ERROR;

  currentToken = NULL;
  lookAhead = getValidToken();

  compileProgram();

  free(currentToken);
  free(lookAhead);
  closeInputStream();
  return IO_SUCCESS;

}
/*typedef enum {
  TK_NONE, TK_IDENT, TK_NUMBER, TK_CHAR, TK_EOF,

  KW_PROGRAM, KW_CONST, KW_TYPE, KW_VAR,
  KW_INTEGER, KW_CHAR, KW_ARRAY, KW_OF, 
  KW_FUNCTION, KW_PROCEDURE,
  KW_BEGIN, KW_END, KW_CALL,
  KW_IF, KW_THEN, KW_ELSE,
  KW_WHILE, KW_DO, KW_FOR, KW_TO,

  SB_SEMICOLON, SB_COLON, SB_PERIOD, SB_COMMA,
  SB_ASSIGN, SB_EQ, SB_NEQ, SB_LT, SB_LE, SB_GT, SB_GE,
  SB_PLUS, SB_MINUS, SB_TIMES, SB_SLASH,
  SB_LPAR, SB_RPAR, SB_LSEL, SB_RSEL
} TokenType
typedef enum {
  ERR_ENDOFCOMMENT,
  ERR_IDENTTOOLONG,
  ERR_INVALIDCHARCONSTANT,
  ERR_INVALIDSYMBOL,
  ERR_INVALIDCONSTANT,
  ERR_INVALIDTYPE,
  ERR_INVALIDBASICTYPE,
  ERR_INVALIDPARAM,
  ERR_INVALIDSTATEMENT,
  ERR_INVALIDARGUMENTS,
  ERR_INVALIDCOMPARATOR,
  ERR_INVALIDEXPRESSION,
  ERR_INVALIDTERM,
  ERR_INVALIDFACTOR,
  ERR_INVALIDCONSTDECL,
  ERR_INVALIDTYPEDECL,
  ERR_INVALIDVARDECL,
  ERR_INVALIDSUBDECL,
} ErrorCode;
*/